(function() {
    'use strict';

    angular
        .module('taskTrackerApp')
        .controller('CommentDialogController', CommentDialogController);

    CommentDialogController.$inject = ['$timeout', '$scope', '$uibModalInstance', 'taskId', 'entity', 'Comment', 'Principal', 'FullName'];

    function CommentDialogController ($timeout, $scope, $uibModalInstance, taskId, entity, Comment, Principal, FullName) {
        var vm = this;

        vm.comment = entity;
        vm.clear = clear;
        vm.save = save;

        if (!vm.comment.id) {
            vm.comment.task = {
                id: taskId
            }
        }

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.comment.jhi_user = FullName.addFullName(account);
            });
        }

        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.comment.id !== null) {
                Comment.update(vm.comment, onSaveSuccess, onSaveError);
            } else {
                Comment.save(vm.comment, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taskTrackerApp:commentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
