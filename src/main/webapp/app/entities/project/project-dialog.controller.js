(function() {
    'use strict';

    angular
        .module('taskTrackerApp')
        .controller('ProjectDialogController', ProjectDialogController);

    ProjectDialogController.$inject = ['$timeout', '$scope', '$uibModalInstance', 'entity', 'Project', 'User', 'Task', 'FullName'];

    function ProjectDialogController ($timeout, $scope, $uibModalInstance, entity, Project, User, Task, FullName) {
        var vm = this;

        vm.project = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = [];

        addUsersFullNames();

        function addUsersFullNames() {
            vm.users = User.query(function (users) {
                vm.users = users.map(FullName.addFullName);
            });
        }

        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.project.id !== null) {
                Project.update(vm.project, onSaveSuccess, onSaveError);
            } else {
                Project.save(vm.project, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taskTrackerApp:projectUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
