(function() {
    'use strict';

    angular
        .module('taskTrackerApp')
        .component('projectTasks', {
            controller: TaskController,
            templateUrl: 'app/entities/project/project-tasks.component.html',
            bindings: {
                tasks: '=',
                projectId: '='
            }
        });

    TaskController.$inject = [];

    function TaskController() {
    }
})();
