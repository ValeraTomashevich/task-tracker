(function() {
    'use strict';

    angular
        .module('taskTrackerApp')
        .controller('ProjectDetailController', ProjectDetailController);

    ProjectDetailController.$inject = ['$scope', '$rootScope', 'previousState', 'entity', 'Task', 'Principal', 'FullName'];

    function ProjectDetailController($scope, $rootScope, previousState, entity, Task, Principal, FullName) {
        var vm = this;

        vm.project = entity;
        vm.previousState = previousState.name;
        vm.filterTasks = filterTasks;
        vm.currentTasks = vm.project.tasks;
        vm.isTasksFiltered = false;
        var uniqueTasks = [];
        var account = {};

        Principal.identity().then(function(currentAccount) {
            account = currentAccount;
        });

        function filterTasks(isTasksFiltered) {
            if (isTasksFiltered) {
                if (uniqueTasks.length > 0) {
                    vm.currentTasks = uniqueTasks;
                }else {
                    Task.query({projectId: vm.project.id, userId: account.id}).$promise.then(function (tasks) {
                        vm.currentTasks = tasks;
                        uniqueTasks = tasks;
                    })
                }
            } else {
                vm.currentTasks = vm.project.tasks;
            }
        }

        vm.project.jhi_users = vm.project.jhi_users.map(FullName.addFullName);

        // function addFullName(user) {
        //     user.fullName = user.firstName + ' ' + user.lastName;
        //     return user;
        // }

        var unsubscribe = $rootScope.$on('taskTrackerApp:projectUpdate', function(event, result) {
            vm.project = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
