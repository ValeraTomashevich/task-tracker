(function() {
    'use strict';

    angular
        .module('taskTrackerApp')
        .component('taskComments', {
            controller: CommentController,
            templateUrl: 'app/entities/task/task-comments.component.html',
            bindings: {
                comments: '='
            }
        });

    CommentController.$inject = [];

    function CommentController() {
    }
})();
