(function() {
    'use strict';

    angular
        .module('taskTrackerApp')
        .controller('TaskDialogController', TaskDialogController);

    TaskDialogController.$inject = ['$timeout', '$scope', '$uibModalInstance', 'projectId', 'entity', 'Task', 'User', 'FullName', 'TaskStatus', 'Principal'];

    function TaskDialogController ($timeout, $scope, $uibModalInstance, projectId, entity, Task, User, FullName, TaskStatus, Principal) {
        var vm = this;

        vm.task = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = [];
        vm.statuses = TaskStatus();

        loadAllAssignees();
        getCurrentUser();

        function loadAllAssignees() {
            User.query().$promise.then(function (users) {
                vm.users = users.map(FullName.addFullName);
            })
        }

        function getCurrentUser() {
            Principal.identity().then(function(account) {
                vm.task.reporter = account;
            });
        }

        if (!vm.task.id) {
            vm.task.project = {
                id: projectId
            };
        }

        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.task.id !== null) {
                Task.update(vm.task, onSaveSuccess, onSaveError);
            } else {
                Task.save(vm.task, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('taskTrackerApp:taskUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
