(function() {
    'use strict';
    angular
        .module('taskTrackerApp')
        .constant('TaskStatus', TaskStatus);

    function TaskStatus () {
        return [
            'WAITING',
            'IMPLEMENTATION',
            'VERIFYING',
            'RELEASING'
        ]
    }
})();
