(function() {
    'use strict';

    angular
        .module('taskTrackerApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
