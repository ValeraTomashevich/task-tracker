(function() {
    'use strict';

    angular
        .module('taskTrackerApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    var authorities = ['ROLE_MANAGER', 'ROLE_DEVELOPER'];

    function stateConfig ($stateProvider) {
        $stateProvider.state('docs', {
            parent: 'admin',
            url: '/docs',
            data: {
                authorities: authorities,
                pageTitle: 'API'
            },
            views: {
                'content@': {
                    templateUrl: 'app/admin/docs/docs.html'
                }
            }
        });
    }
})();
