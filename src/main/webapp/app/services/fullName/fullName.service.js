(function () {
    'use strict';

    angular
        .module('taskTrackerApp')
        .factory('FullName', FullName);

    function FullName () {
        var service = {
            'addFullName': function(user) {
                user.fullName = user.firstName + ' ' + user.lastName;
                return user;
            }
        };
        return service;
    }
})();
