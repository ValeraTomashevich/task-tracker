(function() {
    'use strict';

    angular
        .module('taskTrackerApp')
        .factory('Authorities', Authorities);

    Authorities.$inject = ['$resource'];

    function Authorities ($resource) {
        var service = $resource('/api/authorities', {}, {
            'get': { method: 'GET', isArray: true
            }
        });

        return service;
    }
})();
