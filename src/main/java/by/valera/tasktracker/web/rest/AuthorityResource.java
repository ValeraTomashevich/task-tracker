package by.valera.tasktracker.web.rest;

import by.valera.tasktracker.domain.Authority;
import by.valera.tasktracker.repository.AuthorityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by valera on 17.4.17.
 */
@RestController
@RequestMapping("/api")
public class AuthorityResource {

    private final Logger log = LoggerFactory.getLogger(AuthorityResource.class);

    private final AuthorityRepository authorityRepository;

    public AuthorityResource(AuthorityRepository authorityRepository) {

        this.authorityRepository = authorityRepository;
    }

    /**
     * GET /authorities : get all authorities
     * @return the ResponseEntity with status 200 (OK) and with body all authorities
     */
    @GetMapping("/authorities")
    public ResponseEntity<List<Authority>> getAllAuthorities() {
        final List<Authority> authorities = authorityRepository.findAll();
        return new ResponseEntity<>(authorities, new HttpHeaders(), HttpStatus.OK);
    }

}
