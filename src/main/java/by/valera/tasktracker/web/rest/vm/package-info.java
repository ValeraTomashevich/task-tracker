/**
 * View Models used by Spring MVC REST controllers.
 */
package by.valera.tasktracker.web.rest.vm;
