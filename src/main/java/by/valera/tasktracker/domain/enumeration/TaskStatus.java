package by.valera.tasktracker.domain.enumeration;

/**
 * The TaskStatus enumeration.
 */
public enum TaskStatus {
    WAITING, IMPLEMENTATION, VERIFYING, RELEASING
}
