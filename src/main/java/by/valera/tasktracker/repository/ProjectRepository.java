package by.valera.tasktracker.repository;

import by.valera.tasktracker.domain.Project;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Project entity.
 */
@SuppressWarnings("unused")
public interface ProjectRepository extends JpaRepository<Project,Long> {

    @Query("select distinct project from Project project left join fetch project.jhi_users")
    List<Project> findAllWithEagerRelationships();

    @Query("select project from Project project left join fetch project.jhi_users where project.id =:id")
    Project findOneWithEagerRelationships(@Param("id") Long id);

    @EntityGraph(attributePaths = {"jhi_users", "tasks"})
    Project findOneWithTasksAndUsersById(Long id);
}
