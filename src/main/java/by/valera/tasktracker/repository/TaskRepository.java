package by.valera.tasktracker.repository;

import by.valera.tasktracker.domain.Task;

import by.valera.tasktracker.domain.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Spring Data JPA repository for the Task entity.
 */
@SuppressWarnings("unused")
@Transactional
public interface TaskRepository extends JpaRepository<Task,Long> {

    @EntityGraph(attributePaths = {"project", "assignees", "comments"})
    Task findOneWithProjectUsersCommentsById(Long id);

    @Query("select task from Task task left join fetch task.assignees")
    List<Task> findAllWithAssignees();

    @Query(value = "select * from Task as tt " +
        "join TASK_PROJECT as tp on tt.id = tp.TASK_ID " +
        "join TASK_JHI_USER as tju on tt.id = tju.TASKS_ID " +
        "where ASSIGNEES_ID = :userId and PROJECT_ID = :projectId and tt.id in " +
        "(select t.ID from (TASK as t inner join TASK_JHI_USER AS tu on t.ID = tu.TASKS_ID) " +
        "inner join JHI_USER as u on tu.ASSIGNEES_ID = u.ID  group by TASKS_ID  having count(TASKS_ID) =1)", nativeQuery = true)
    List<Task> findAllUnicTasksForCurrentUserInCurrentProject(@Param("userId") Long userId, @Param("projectId") Long projectId);
}
