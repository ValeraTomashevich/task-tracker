package by.valera.tasktracker.repository;

import by.valera.tasktracker.domain.Comment;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Comment entity.
 */
@SuppressWarnings("unused")
public interface CommentRepository extends JpaRepository<Comment,Long> {

    @Query("select comment from Comment comment where comment.jhi_user.login = ?#{principal.username}")
    List<Comment> findByJhi_userIsCurrentUser();

}
