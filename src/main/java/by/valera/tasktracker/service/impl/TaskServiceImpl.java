package by.valera.tasktracker.service.impl;

import by.valera.tasktracker.domain.User;
import by.valera.tasktracker.service.TaskService;
import by.valera.tasktracker.domain.Task;
import by.valera.tasktracker.repository.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Implementation for managing Task.
 */
@Service
@Transactional
public class TaskServiceImpl implements TaskService{

    private final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Save a task.
     *
     * @param task the entity to save
     * @return the persisted entity
     */
    @Override
    public Task save(Task task) {
        log.debug("Request to save Task : {}", task);
        Task result = taskRepository.save(task);
        return result;
    }

    /**
     *  Get all the tasks.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Task> findAll() {
        log.debug("Request to get all Tasks");
        List<Task> result = taskRepository.findAllWithAssignees();

        return result;
    }

    /**
     *  Get one task by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Task findOne(Long id) {
        log.debug("Request to get Task : {}", id);
        Task task = taskRepository.findOne(id);
        return task;
    }

    /**
     *  Delete the  task by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Task : {}", id);
        taskRepository.delete(id);
    }

    @Override
    public Task getOneWithProjectUsersCommentsById(Long id) {
        return taskRepository.findOneWithProjectUsersCommentsById(id);
    }

    @Override
    public List<Task> getUnicTasks(Long userId, Long projectId) {
        return taskRepository.findAllUnicTasksForCurrentUserInCurrentProject(userId, projectId);
    }
}
